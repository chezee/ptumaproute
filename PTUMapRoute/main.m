//
//  main.m
//  PTUMapRoute
//
//  Created by Ilya on 22/2/17.
//  Copyright © 2017 Ilya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
