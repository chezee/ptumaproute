//
//  AppDelegate.h
//  PTUMapRoute
//
//  Created by Ilya on 22/2/17.
//  Copyright © 2017 Ilya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

